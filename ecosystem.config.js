module.exports = {
    apps: [{
      name: "app",
      script: "./app.js",
      env: {
        NODE_ENV: "development",
        PORT: 3000,
        SAY: "Hello dev!"
      },
      env_staging: {
        NODE_ENV: "staging",
        PORT: 3000,
        SAY: "Hello stag!"
      },
      env_production: {
        NODE_ENV: "production",
        PORT: 3000,
        SAY: "Hello prod!"
      }
    }]
  }